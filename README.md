# Ontodia as a Vue.js v2 component

This repository contains the initial code to provide Ontodia (RDF visualisation), which is written in React, as a Vue.js (v2) component. This wasn't finished, but works rudimentarily.

import via `import { ontodia } from 'vue-ontodia/src/main.js'`

See `src/demo.js` for an example.

There is no new version of Ontodia (yet), but the direct integration of Ontodia inside the EAs.LiT frontend is more developed than this repository.

## Drawbacks
Due to the big size of the built module, large dependecies were defined as external dependencies:
* ontodia
* rdf-parser-jsonld
* rdf-parser-n3
* react
* react-dom
