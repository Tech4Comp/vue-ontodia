module.exports = {
  configureWebpack: config => {
    if (process.env.TARGET === 'lib') {
      config.externals = [
        'ontodia',
        'rdf-parser-jsonld',
        'rdf-parser-n3',
        'react',
        'react-dom'
      ]
    }
  }
}
